'use strict';

const tabs = [
  // X=closed O=open +=optional H=half
  // two semitones lower
  { tab: "X XXX XXXX", noteC: 'C4',  freqC: 261.63, noteD: 'D4',  freqD: 293.66 },
  { tab: "X XXX XXXH", noteC: 'C#4', freqC: 277.18, noteD: 'D#4', freqD: 311.13 },
  { tab: "X XXX XXXO", noteC: 'D4',  freqC: 293.66, noteD: 'E4',  freqD: 329.63 },
  { tab: "X XXX XXOO", noteC: 'D#4', freqC: 311.13, noteD: 'F4',  freqD: 349.23 },
  { tab: "X XXX XOOO", noteC: 'E4',  freqC: 329.63, noteD: 'F#',  freqD: 369.99 },
  { tab: "X XXX OOO+", noteC: 'F4',  freqC: 349.23, noteD: 'G',   freqD: 392.00 },
  { tab: "X XXO OOO+", noteC: 'F#4', freqC: 369.99, noteD: 'G#',  freqD: 415.30 },
  { tab: "X XOO OOO+", noteC: 'G4',  freqC: 392.00, noteD: 'A',   freqD: 440.00 },
  { tab: "X O+O OO++", noteC: 'G#4', freqC: 415.30, noteD: 'A#',  freqD: 466.16 },
  { tab: "O XXX XXX+", noteC: 'A4',  freqC: 440.00, noteD: 'B',   freqD: 493.88 },
  { tab: "O OOO OOO+", noteC: 'A#4', freqC: 466.16, noteD: 'C',   freqD: 523.25 },


  { tab: "X XXX XXXX", noteC: 'C5',  freqC: 523.25, noteD: 'D5', freqD: 587.33 },
  { tab: "X XXX XXXH", noteC: 'C#5', freqC: 554.37, noteD: 'D#5', freqD: 622.25 },
  { tab: "X XXX XXXO", noteC: 'D5',  freqC: 587.33, noteD: 'E5', freqD: 659.26 },
  { tab: "X XXX XXOO", noteC: 'D#5', freqC: 622.25, noteD: 'F5', freqD: 698.46 },
  { tab: "X XXX XOOO", noteC: 'E5',  freqC: 659.26, noteD: 'F#5', freqD: 739.99 },
  { tab: "X XXX OOO+", noteC: 'F5',  freqC: 698.46, noteD: 'G5', freqD: 783.99 },
  { tab: "X XXO OOO+", noteC: 'F#5', freqC: 739.99, noteD: 'G#5', freqD: 830.61 },
  { tab: "X XOO OOO+", noteC: 'G5',  freqC: 783.99, noteD: 'A5', freqD: 880.00 },
  { tab: "X O+O OOO+", noteC: 'G#5', freqC: 830.61, noteD: 'A#5', freqD: 932.33 },


  { tab: "X XXX XXXX", noteC: 'G5',  freqC: 783.99, noteD: 'A5', freqD: 880.00 },
  { tab: "X XXX XXXH", noteC: 'G#5', freqC: 830.61, noteD: 'A#5', freqD: 932.33 },
  { tab: "X XXX XXXO", noteC: 'A5',  freqC: 880.00, noteD: 'B5', freqD: 987.77 },
  { tab: "X XXX XXOO", noteC: 'A#5', freqC: 932.33, noteD: 'C6', freqD: 1046.50 },
  { tab: "X XXX XOOO", noteC: 'B5',  freqC: 987.77, noteD: 'C#6', freqD: 1108.73 },
  { tab: "X XXX OOO+", noteC: 'C6',  freqC: 1046.50, noteD: 'D6', freqD: 1174.66 },
  { tab: "X XXO OOO+", noteC: 'C#6', freqC: 1108.73, noteD: 'D#6', freqD: 1244.51 },
  { tab: "X XOO OOO+", noteC: 'D6',  freqC: 1174.66, noteD: 'E6', freqD: 1318.51 },
  { tab: "X O+O OOO+", noteC: 'D#6', freqC: 1244.51, noteD: 'F6', freqD: 1396.91 },
  { tab: "O XOO OOO+", noteC: 'E6',  freqC: 1318.51, noteD: 'F#6', freqD: 1479.98 },


  { tab: "X XXX XOOX", noteC: 'E6 - ?',freqC: 1318.51, noteD: 'F#6', freqD: 1479.88 },
  { tab: "X XXX OOOX", noteC: 'F6',    freqC: 1396.91, noteD: 'G6', freqD: 1567.98 },
  { tab: "X XXO OOOX", noteC: 'F#6',   freqC: 1479.98, noteD: 'G#6', freqD: 1661.22 },
  { tab: "X XOO OOOX", noteC: 'G6',    freqC: 1567.98, noteD: 'A6', freqD: 1760.00 },
  { tab: "X XOX XOOX", noteC: 'G#6',   freqC: 1661.22, noteD: 'A#6', freqD: 1864.66 },
  { tab: "X OXX OOOX", noteC: 'A6',    freqC: 1760.00, noteD: 'B6', freqD: 1975.53 },
  { tab: "X XXX XXXO", noteC: 'A#6',   freqC: 1864.66, noteD: 'C7', freqD: 2093.00 },
  { tab: "X XXO XXOO", noteC: 'B6',    freqC: 1975.53, noteD: 'C#7', freqD: 2217.46 },
];
