'use strict';

class Kaval {
  // Methods:
  constructor(canvas_id, is_mobile=false) {
    this.canvas = document.getElementById(canvas_id);
    this.c = this.canvas.getContext('2d');

    this.top = 20; // This is the unit size
    if (is_mobile) {
      this.top = 40;
    }
    this.left = this.top;
    //this.circle_radius = this.top/3;
    this.canvas.width = this.left*3;
    this.canvas.height = this.top*7;
    this.circle_radius = this.canvas.height/(8*3);
    this.tab = null;
  }

  //////////////// PUBLIC ////////////////////
  Draw(tab) {
    // X=closed, O=open, +=optional, H=half
    // Note: Spaces are ignored, and can be as many or as few none at all
    // tab: "X OOO OX+H"
    this.tab = tab;
    const c = this.c;
    const r = this.circle_radius + 2; // TODO: 2 hardcoded
    const x = this.canvas.width/2;
    const h = this.canvas.height;
    const o = 20; // textOffset
    const H = h - o;
    const pos = [
      { x: x-r,   y: r + 0/8*H },

      { x: x,     y: r + 1/8*H },
      { x: x,     y: r + 2/8*H },
      { x: x,     y: r + 3/8*H },

      { x: x,     y: r + 4/8*H },
      { x: x,     y: r + 5/8*H },
      { x: x,     y: r + 6/8*H },
      { x: x,     y: r + 7/8*H },
    ];

    let i = 0;
    for (const c in tab) {
      if (tab[c] == ' ') {
        continue;
      }
      this.DrawHole(pos[i], tab[c]);
      i++;
    }

    // Draw divider
    c.beginPath();
    c.moveTo(this.canvas.width/2 - r, H/2);
    c.lineTo(this.canvas.width/2 + r, H/2);
    c.stroke();
  }


  DrawText(text) {
    let c = this.c;
    const h = this.canvas.width;
    c.font = '0.8em sans';
    let t = c.measureText(text);
    c.fillText(text, h/2-t.width/2, this.canvas.height-2);
  };


  DrawHole(pos, type='X') {
    // Draw a circle at given coords (full or not)
    // type: X=closed, O=open, +=optional, H=half
    // pos: { x, y }
    // x = 1,2,3
    // y = 1,...,6
    const c = this.c;

    if (['X', '+'].includes(type)) {
      // closed/optional: both are filled circles, only colour is different
      c.beginPath();
      c.arc(pos.x, pos.y, this.circle_radius, 0, 2 * Math.PI);
      if (type == '+') {
        c.fillStyle = 'silver'; // TODO: change to grey or silver
      }
      c.fill();
      c.fillStyle = 'black';
    }
    if (type == 'H') {
      // half-closed, draw half-circle
      c.beginPath();
      c.arc(pos.x, pos.y, this.circle_radius, Math.PI, 2 * Math.PI);
      c.fill();
    }

    // Bounding circle
    c.beginPath();
    //x = this.left*x;
    //y = this.top*(y+1) ;
    c.arc(pos.x, pos.y, this.circle_radius, 0, 2 * Math.PI);
    c.stroke();
  }


  _getId(id) {
    return document.getElementById(id);
  }
}; //class TrumpetFingeringCard
