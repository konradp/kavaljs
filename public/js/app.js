const keys = [ 'C', 'D' ];
let key = 'D';
let cards = [];
let scale_notes_only = false;
let synth = new Tinysynth();
let is_mobile = false;
window.addEventListener('resize', checkMobile);



window.onload = function() {
  checkMobile();
  DrawCards();
  DrawKeyPicker();

  // Scale-notes-only checkbox
  let scale_notes_picker = _getId('scale-notes-only');
  scale_notes_picker.addEventListener('change', OnScaleNotesOnlyOnOff, false);
}


function DrawCards() {
  const div = _getId('main');
  div.innerHTML = '';
  for (i in tabs) {
    const c = _new('canvas');
    c.className = 'card';

    if (i == 11 || i == 20 || i == 30) {
      const a = _new('div');
      a.style = 'flex-basis: 100%; height:0';
      div.appendChild(a);
    }

    c.id = i;
    div.appendChild(c);
    cards.push(c);
    const card = new Kaval(i, is_mobile);
    card.Draw(tabs[i]['tab']);
    card.DrawText(tabs[i][`note${key}`]);
    c.setAttribute('freq', tabs[i][`freq${key}`]);
    c.addEventListener('mousedown', (e) => {
      let note = e.target.getAttribute('freq');
      synth.noteOn(parseFloat(note), duration=500);
    });
  }
}


function DrawKeyPicker() {
  let keyPicker = _getId('key');
  for (i in keys) {
    let opt = _new('option');
    opt.innerHTML = keys[i];
    keyPicker.appendChild(opt);
  }
  keyPicker.value = key;
  keyPicker.addEventListener('change', OnKeyChange, false);
}


function checkMobile() {
  let before = is_mobile;
  if (navigator.userAgent.includes('Mobile')
    || 'ontouchstart' in document.documentElement
  ) {
    is_mobile = true;
  } else {
    is_mobile = false;
  }
  if (is_mobile != before) {
    DrawCards();
  }
};


// Handlers
function OnScaleNotesOnlyOnOff(event) {
  scale_notes_only = event.target.checked;
}


function OnKeyChange(event) {
  key = event.target.value;
  DrawCards();
}


// Helpers
function _getId(id) {
  return document.getElementById(id);
}
function _new(el) {
  return document.createElement(el);
}
