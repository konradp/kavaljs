var express = require('express');
var app = express();
var port = 5000;
app.use(express.static('public'))
app.get('/', (req, res) => res.sendFile(__dirname + '/public/index.html'))
app.listen(port, () => console.log('Listening on http://localhost:'+port))
